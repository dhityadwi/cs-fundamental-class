const { provinces, regencies } = require ('./location');

function findCityMoreThanNWords(n) {
     
    let word = []

    for(let regency of regencies) {
        if (regency.name.split(" ").length == n) {
            word.push(regency.name);
        }
    }
    
    return word;
}

console.log(findCityMoreThanNWords(3));
function romanToNum(n) {
    
    let romanCode = {
      I: 1,
      V: 5,
      X: 10,
      L: 50,
      C: 100,
      D: 500,
      M: 1000
    }

    let total = 0;

    for (let i = 0; i < n.length; i++) {
        // console.log(i);
      let currentNum = romanCode[n.charAt(i)];
         console.log(currentNum);
      let nextNum = romanCode[n.charAt(i + 1)];
        console.log(nextNum);

      if (nextNum) {
        if (currentNum >= nextNum) {
          total = total + currentNum;
        //   console.log(total);
        } else {
          total = total + (nextNum - currentNum);
          i++;
        }
      } else {
        total = total + currentNum;
      }
    }

    return total;
};

console.log(romanToNum("MCDXL"));